///////////////////////////////////////////
//                                       //
//           S C I A N T I X             //
//           ---------------             //
//                                       //
//  Version: 1.4                         //
//  Year   : 2019                        //
//  Authors: D. Pizzocri and T. Barani   //
//                                       //
///////////////////////////////////////////

#include <cmath>

namespace ManufacturedSolution
{
  void SinExpPow(double constants[], double time, double &solution, double &derivative);

  void CosExp(double constants[], double time, double &solution, double &derivative);

  void Vector1x2(double x[]);

  void Vector1x3(double x[]);

  void Vector1xN(int N, double x[]);
};
