///////////////////////////////////////////
//                                       //
//           S C I A N T I X             //
//           ---------------             //
//                                       //
//  Version: 0.1                         //
//  Year   : 2019                        //
//  Authors: D. Pizzocri and T. Barani   //
//                                       //
///////////////////////////////////////////

#include "MainVariables.h"
#include "ErrorMessages.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <limits>

void OutputWriting( );
