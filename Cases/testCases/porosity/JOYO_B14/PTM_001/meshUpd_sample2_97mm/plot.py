import numpy as np
import matplotlib.pyplot as plt

dataBIS = np.genfromtxt("./dataBISON.csv",delimiter=",")
data = np.genfromtxt("./postProcessing/sample/251280/radial_porosity.csv", 
                     skip_header=1, 
                     delimiter=",")

normRadius = data[:,0]/max(data[:,0])

plt.figure()
plt.scatter(dataBIS[:,0],dataBIS[:,1], label='BISON', color='k')
plt.plot(normRadius,data[:,1], label='OFFBEAT', color='r')
plt.xlabel(r"$r/r_{out}$ (-)")
plt.ylabel("porosity (-)")
plt.legend()
plt.savefig("porosity.png")

writeArray = np.column_stack((normRadius,data[:,1]))
np.savetxt("porosity.csv",writeArray)