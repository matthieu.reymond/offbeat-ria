/*--------------------------------*- C++ -*----------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Version:  9
     \\/     M anipulation  |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version         2;
    format          ascii;
    class           dictionary;
    location        "constant";
    object          solverDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

thermalSolver   solidConduction;

mechanicsSolver smallStrain;

neutronicsSolver fromLatestTime;
elementTransport        fromLatestTime;

materialProperties byZone;

rheology        byMaterial;

heatSource      timeDependentLhgr;

burnup          fromLatestTime;

fastFlux        timeDependentAxialProfile;

gapGas          FRAPCON;

fgr             SCIANTIX;

corrosion       fromLatestTime;

sliceMapper     autoAxialSlices;

globalOptions
{
    pinDirection    ( 0 0 1 );
    reactorType     "LWR";
}

thermalSolverOptions
{
    heatFluxSummary off;
}

rheologyOptions
{
    thermalExpansion on;
}

mechanicsSolverOptions
{
    forceSummary    off;
    cylindricalStress on;
    multiMaterialCorrection
    {
        type            uniform;
        defaultWeights  0.9;
    }
}

fgrOptions
{
    nFrequency      1;
    relax           1;
}

gapGasOptions
{
    gapPatches      ( fuelOuter cladInner );
    holePatches     ( );
    topFuelPatches  ( fuelTop );
    bottomFuelPatches ( fuelBottom );
    gapVolumeOffset 0;
    gasReserveVolume 0;
    gasReserveTemperature 290;
}

heatSourceOptions
{
    timePoints      ( 0 3600 1.26e+08 );
    lhgr            ( 0 23000 18000 );
    timeInterpolationMethod linear;
    axialProfile
    {
        type            timeDependentTabulated;
        #include        "axialProfile"
        axialInterpolationMethod linear;
        burnupInterpolationMethod linear;
    }
    radialProfile
    {
        type            flat;
    }
    materials       ( fuel );
}

fastFluxOptions
{
    timePoints      ( 0 3600 1.26e+08 );
    fastFlux        ( 0 2e+13 1e+13 );
    timeInterpolationMethod linear;
    axialProfile
    {
        type            flat;
    }
    materials       ( fuel cladding );
}

materials
{
    fuel
    {
        material        UO2;
        Tref            Tref [ 0 0 0 1 0 ] 293;
        densificationModel UO2FRAPCON;
        swellingModel   UO2FRAPCON;
        relocationModel UO2FRAPCON;
        enrichment      0.045;
        rGrain          2.8e-05;
        GdContent       0;
        theoreticalDensity 10960;
        densityFraction 0.95;
        dishFraction    0;
        resinteringDensityChange 0.3;
        GapCold         0.00013;
        DiamCold        0.009;
        recoveryFraction 0.5;
        outerPatch      "fuelOuter";
        isotropicCracking on;
        nCracksMax      12;
        rheologyModel   elasticity;
    }
    cladding
    {
        material        zircaloy;
        Tref            Tref [ 0 0 0 1 0 ] 293;
        PoissonRatioModel ZyConstant;
        rheologyModel   misesPlasticCreep;
        rheologyModelOptions
        {
            plasticStrainVsYieldStress table ( ( 0 2.5e+08 ) );
            creepModel      LimbackCreepModel;
            relax           1;
        }
    }
}


// ************************************************************************* //
