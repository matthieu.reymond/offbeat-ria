/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.3.0                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       volVectorField;
    object      D;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

dimensions          [0 1 0 0 0 0 0];

internalField       uniform (0 0 0);

boundaryField
{
    ".*Front|.*Back"
    {
        type            wedge;
    }
    
    "cladOuter|cladTop"
    {
        type            coolantPressure;

        coolantPressureList    
        {
            file           "$FOAM_CASE/constant/systemPressure";
            outOfBounds     clamp;
        };

        relax           1.0;
        value           $internalField;
    }

    "fuelOuter|cladInner"
    {
        type                    gapContact;
        patchType               regionCoupledOFFBEAT;

        penaltyFactor           1;
        penaltyFactorFriction   0.01;
        frictionCoefficient     1;

        relax                   1.0;
        relaxInterfacePressure  0.01;
        relaxFriction           0.01;
        
        value                   $internalField;
    }

    cladBottom
    {
        type                    fixedValue;
        value                   $internalField;        
    }

    "fuelTop|topCapInner"
    {
        type                  plenumSpringPressure;

        // Names of fuel top patches
        fuelTopPatches (fuelTop);

        // Names of cap inner patches 
        // If the cap is missing from the model, the top 
        // cladding pach should be used instead
        topCapInnerPatches (topCapInner);

        springModulus         3.5e3;
        springPreCompression  0.0;

        relax           1.0;

        // Plane strain approximation avoids end effect jumps due to coarse axial
        // discretization
        planeStrain     off;

        value           $internalField;
    }

    "fuelBottom|bottomCapInner"
    {
        type            implicitGapContact;
        patchType       regionCoupledOFFBEAT;

        glued on;

        penaltyFactor             1e6;
        penaltyFactorFriction    0.01;
        frictionCoefficient         0;
        penaltyFactorDashpot      0.0;

        relax 0.1;
        
        traction        uniform (0 0 0);
        pressure        uniform 2.25e6;
        value           $internalField;        
    }

    "dishChamferTop_.*|dishChamferBottom_.*"
    {
        type            gapPressure;
        value           $internalField;  
    }

    "fuelTop_.*|fuelBottom_.*"
    {
        type            implicitGapContact;
        patchType       regionCoupledOFFBEAT;

        glued off;

        penaltyFactor             1e6;
        penaltyFactorFriction    0.01;
        frictionCoefficient        0;
        penaltyFactorDashpot      0.0;

        relax 0.1;
        
        traction        uniform (0 0 0);
        pressure        uniform 2.25e6;
        value           $internalField;        
    }

}

// ************************************************************************* //
