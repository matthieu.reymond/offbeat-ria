/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.3.0                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       volVectorField;
    object      D;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

dimensions          [0 1 0 0 0 0 0];

internalField       uniform (0 0 0);

boundaryField
{
    ".*Front|.*Back"
    {
        type            wedge;
    }

    cladOuter
    {
        type            coolantPressure;

        coolantPressureList    
        {
            file           "$FOAM_CASE/constant/systemPressure";
            outOfBounds     clamp;
        };

        relax           1.0;
        value           $internalField;
    }

    "fuelOuter|cladInner"
    {
        type                    gapContact;
        patchType               regionCoupledOFFBEAT;

        penaltyFactor           1;
        penaltyFactorFriction   0.01;
        frictionCoefficient     0;

        relax                   1.0;
        relaxInterfacePressure  0.01;
        relaxFriction           0.01;
        
        value                   $internalField;
    }

    "cladBottom|fuelBottom"
    {
        type            fixedDisplacementZeroShear;
        value           $internalField;        
    }

    fuelTop
    {
        type                  ;

        // Names of fuel top patches
        fuelTopPatches (fuelTop);

        // Names of cap inner patches 
        // If the cap is missing from the model, the top 
        // cladding pach should be used instead
        topCapInnerPatches (cladTop);

        springModulus         3.5e3;
        springPreCompression  0.0;

        relax           1.0;

        // Plane strain approximation avoids end effect jumps due to coarse axial
        // discretization
        planeStrain     on;

        value           $internalField;
    }

    cladTop
    {
        type                    ;

        topCapOuterRadius       5.26e-3;
        topCapInnerRadius       4.56e-3;      

        coolantPressureList    
        {
            file           "$FOAM_CASE/constant/systemPressure";
            outOfBounds     clamp;
        };

        relax           1.0;

        // Plane strain approximation avoids end effect jumps due to coarse axial
        // discretization
        planeStrain     on;

        value           $internalField;
    }

}

// ************************************************************************* //
