/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.3.0                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant";
    object      solverDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

//- Thermal and Mechanical solver selection:
thermalSolver           solidConduction;
mechanicsSolver         smallStrain;
neutronicsSolver        fromLatestTime;
elementTransport        fromLatestTime;

//- Material and rhelogy treatment:
materialProperties      byZone;
rheology                byMaterial;

heatSource              fromLatestTime;
burnup                  fromLatestTime;
fastFlux                timeDependentAxialProfile;
corrosion               fromLatestTime;
gapGas                  none;
fgr                     none;
sliceMapper             autoAxialSlices;
corrosion               fromLatestTime;

globalOptions
{
    pinDirection            (0 0 1);
    reactorType             "LWR";
}

thermalSolverOptions
{
    heatFluxSummary     off;
}

rheologyOptions
{
    thermalExpansion off;
}

mechanicsSolverOptions
{
    forceSummary        on;
    cylindricalStress   on;

    // multiMaterialCorrection
    // {
    //     type                    uniform;
    //     defaultWeights          1;
    // }
}

fgrOptions
{
    nFrequency  1;
    relax       1;
}

gapGasOptions
{
    gapPatches ( fuelOuter cladInner );
    holePatches ();
    topFuelPatches    ( fuelTop);
    bottomFuelPatches ( fuelBottom);

    gapVolumeOffset 0.0;
    gasReserveVolume 0.0;
    gasReserveTemperature 290;
}

fastFluxOptions
{
    timePoints  ( 0     8e7);
    fastFlux    ( 1.25e14  1.25e14); // n/cm²/s
    timeInterpolationMethod linear;

    axialProfile
    {
        type flat;
    }

    materials (fuel);
}

materials
{
    fuel
    {
      material PyC;
      Tref                        Tref [ 0 0 0 1 0 ] 298.15;

      densityModel                constant;
       conductivityModel           constant;
       heatCapacityModel           constant;
       emissivityModel             constant;
       YoungModulusModel           constant;
       PoissonRatioModel           constant;
      // thermalExpansionModel       PyCPARFUME;
      swellingModel                none;

      emissivity  emissivity      [0 0 0 0 0]     0.0;
      E           E       [1 -1 -2 0 0]   10e9;
      rho         rho     [1 -3 0 0 0]    2200.0;
      Cp          Cp      [0 2 -2 -1 0]   720;
      k           k       [1 1 3 -1 0]    4;
      nu          nu      [0 0 0 0 0]     0.33;

      //rheologyModel               elasticity ;


      rheologyModel misesPlasticCreep;

      rheologyModelOptions
      {
          plasticStrainVsYieldStress table
          (
              (0    1e60)
          );
           // creepModel LimbackCreepModel;
           relax 1.0;
          creepModel PARFUMUEPyCCreepModel;
          fluxConversionFactor       0.91;
      }
    }
}

// ************************************************************************* //
