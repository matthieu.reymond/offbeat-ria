### User documentation for `plenumSpringPressure` fvPatchField class {#fvPatchField_plenumSpringPressureFvPatchVectorField}

For a list of fvPatchField classes specifically developed for OFFBEAT, please refer to the [fieldsAndBCs](fieldsAndBCs.html) page.

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Description </summary> -->

#### Description

The `plenumSpringPressure` boundary condition replicates the counteracting force of the plenum spring.

The total pressure is calculated as the sum of the gap pressure (deriving from a `gapModel`) and from the reaction of the spring.

This in turn obtained from the spring modulus, andh the relative displacement of the surfaces to which the spring is supposed to be attached. 
The shear stress is kept to zero. 

<div class="border-box" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color:#cfe3f5; color:#05134a'>
<b>Note</b>
<br><br>This BC is designed to be applied to the top of the pellet column (i.e. the top most fuel patch) and, if present, the inner surface of the top cladding cap.
<br><br>For models that do not include the top cap (i.e. the cladding ends with an open annular cylindrical surface) but still need to take into account the effect of the spring, the user can select the topCladRingPressure BC.
</div>

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> List of options </summary> -->

#### List of options

The `plenumSpringPressure` is a daughter class of the `tractionDisplacement` boundary condition, so it shares some of its settings.
In particular, this boundary condition requires the user to specify the following parameters in the patch dictionary:

- <b><c>springModulus</c></b> - The modulus of the plenum spring in N/m. It determines the stiffness of the spring.
- <b><c>initialSpringLoading</c></b> - The initial loading of the plenum spring in m. It represents the pre-loading applied to the spring.
- <b><c>fuelTopPatches</c></b> - List of patches attached to the bottom end of the spring (typically this is just the top surface of the fuel column)
- <b><c>topCapInnerPatches</c></b> - List of patches attached to the top end of the spring (typically this is either the inner surface of the top cap, if the cap is present in the model, or the top annular surface of the cladding in case of a model with an open clad)

In addition, the `plenumSpringPressure` boundary condition inherits the following options from the `tractionDisplacement` class:

- <b><c>planeStrain</c></b> - Activate or deactivate the plane strain approximation for the normal stress at the boundary. **By default, it is set to `false`**. When activated, the normal component of the boundary normal gradient (and thus the normal strain) is assumed constant across the last layer of cells. This approximation is useful for 2D r-z meshes, where the axial discretization of the mesh may not capture the rapid variation of the axial stresses close to the boundary.
- <b><c>relax</c></b> - The relaxation factor for the gradient update. **By default, it is set to `1.0`.**
- <b><c>value</c></b> - The initial **displacement** value (not stress value).

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Examples </summary> -->

#### Examples

To apply the `plenumSpringPressure` boundary condition to a given patch, you need to specify its typename in the patch definition in the boundary condition file (typically either `0/D` or `0/DD` in case of a simulation that involves incremental mechanical solvers). 
Here is an example where `plenumSpringPressure` is applied to the `fuelTop` patch with a plenum spring modulus of `1e4` N/m:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
  fuelTop
  {
      type            plenumSpringPressure;

      springModulus   1e4;
      initialSpringLoading 0;

      fuelTopPatches (fuelTop);
      topCapInnerPatches (cladTop);

      // Activate plane strain approximation for the normal stress at the 
      // boundary. It is false by default.
      planeStrain     false;

      // Relaxation factor for the gradient update
      relax           1.0;

      // Initial value
      value           $internalField;
  }

</code></pre>
</div>  


<!-- </details> -->