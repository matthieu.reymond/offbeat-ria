### User documentation for `SCIANTIX` fission gas release class {#fissionGasRelease_fgrSCIANTIX}

For general instructions on the modeling of the fission gas release in OFFBEAT see [here](fgr.html).

<!-- <details> 
	<summary style="font-size: 0.9rem; font-weight: bold;"> Formulation </summary>-->

#### Formulation

The coupling between OFFBEAT and SCIANTIX works as follows:

1 - At every iteration and for every cell of fuel type materials (such as `uo2`), OFFBEAT passes the necessary data to SCIANTIX to perform its calculation. 
This set of data includes:
	- the history variables i.e. local oldTime and current stresses, temperature and fission rate
	- as well as the oldTime values of the SCIANTIX variables for the given cell. These variables are stored in corresponding fields defined in the `fgrSCIANTIX` class (see List of Options for more details).

2 - Using the old time step values as initial conditions, SCIANTIX calculates the current values for its internal variables.

3 - The OFFBEAT fields corresponding to the local SCIANTIX variables are not updated during the outer iterations. However, some of the updated values calculated by SCIANTIX in every cell are used to update relevant quantities, such as the fgr fraction or the gasesous swelling (otherwise the coupling betwee fgr and, for instance, the mechanics would be explicit in time).

4 - At the end of the time step, the steps 1, 2 and 3 are repated once again, this time storing the updated local SCIANTIX variables in the corresponding OFFBEAT fields.

<!-- </details> -->

<!-- <details> 
	<summary style="font-size: 0.9rem; font-weight: bold;"> Usage </summary> -->

#### Usage

To use the `fgrSCIANTIX` fission gas release model in OFFBEAT, you will need to specify it in the `solverDict` dictionary using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	fgr    SCIANTIX;

</code></pre>
</div>

The `fgrSCIANTIX` heat source model requires the user to specify a few additional parameters in the `fgrOptions` sub-dictionary:

- <b><c>relax</c></b> - Relaxation factor applied to inter/intra-gaseous swelling and fgr fraction. It can improve convergence when dealing with strongly coupled simulations. **It is set to 1 by default**.
- <b><c>nFrequency</c></b> - An integer defining how many outer iteration to skip before updating the OFFBEAT/SCIANTIX coupling. It can speed up the simulation (i.e. less SCIANTIX runs) but it should not be too large, as one risks not attaining proper convergence. **It is set to 1 by default**.
- <b><c>addToRegistry</c></b> - Keyword to switch on/off the creation of the SCIANTIX fields as separate volumetric fields. **It is set to `off` by default**.

<div class="border-box" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color:#cfe3f5; color:#05134a'>
<b>Note on `addToRegistry`</b>
<br><br>To avoid the creation of several volumetric fields (e.g. volScalarFields or volVectorFields), by default the SCIANTIX fields are created as traditional fields (scalarFields or vectorFields), using a template function found in globalFields.H
<br>Typically, these fields are read from a regIOObject called `SCIANTIXfields` that is printed at each (writing) time step. This dictionary contains at the top of the file also a summary of the current fgr%. 
<br>If a given field is not present in this dictionary, the fields are created with a default initial value.
<br><br>However, if an IOObject corresponding to one of the fields is present in the initial time step folder, the field is read from this file. This might be useful when performing advanced operations such as using mapping routines available in OpenFOAM. 
<br>In some cases, it may be necessary to have the fields in the registry for specific purposes, such as plotting them in paraFoam, probing them, or mapping them. To control this behavior, the keyword `addToRegistry` can be used in the fgr option dictionary. It allows deciding whether to add the fields to the registry as volumetric fields or not.
</div>

<!-- </details> -->

<!-- <details> 
	 <summary style="font-size: 0.9rem; font-weight: bold;"> Examples </summary> -->

#### Examples

Here is an example of (part of) the `solverDict` file to be used to activate the coupling between OFFBEAT and SCIANTIX .

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	fgr     SCIANTIX; 

	fgrOptions
	{
			//- relax is 1 by default
			relax  1;

			//- nFrequency is 1 by default
			nFrequency  1;

			//- addToRegistry is off by default
			addToRegistry  off;
	}

	//... other physics subdictionaries

	materials
	{
		fuel
		{
			type uo2;

			//... other material specific options

			rGrain 5e-6;

		}
		//... remaining material subdictionaries
	}

</code></pre>
</div>

Additionally, the code SCIANTIX requires the file "input_settings.txt" to be properly set and placed in the case folder. An example of the input_settings file (see SCIANTIX documentation for more details):

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	0 #  verification (0 = no verification)
	1 #  grain growth (0 = none; 1 = ainscough)
	1 #  inert gas behavior (0 = none; 1 = do IGB)
	1 #  gas diff coeff (0 = 0 coeff (trial); 1 = Turnbull, 2 = Matzke (with small OFFBEAT custom addition) )
	1 #  intra bbl evo (1 = Pizzocri et al., 2018; 2 = White, Tucker 1983)
	1 #  intra bubble_radius (1 = Olander&Wongy, 2006, 2= Ronchi 1972; 3 = Dollings 1977; 4 = Losonen 2002; 5 = Spino 2005; )
	1 #  re-solution (1 = Turnbull 1971; 2 = Oldanter 1976)
	1 #  trapping (1 = Ham 1958)
	1 #  nucleation (1 = Baker 1971)
	1 #  DiffSolver (1 = SDA, Pizzocri, 2 = FORMAS)
	1 #  format_out (1 = output.txt, values separated by tabs)
	1 #  gb vac diff coeff (1 = Reynolds and Burton, 1979; 2 = Pastore 2015)
	1 #  gb behavior (1 = Pastore et al., 2013; Barani et al., 2017)
	0 #  gb micro-cracking (0 = none; 1 = Barani et al., 2017)
	0 #  hbs formation (0 = non active)
	0 #  fuel/reactor couple for burnup calculations (0 = UO2/PWR)
	1 #  gas effective (=0) or single atom (=1) diffusion coefficient - OFFBEAT custom addition
	0 #  add boundary gas sweeping (1=on) - OFFBEAT custom addition
	0 #  micro cracking model span parameter (0 = default value of 10; 1 = original value SCIANTIX of 10; 2 = Value from MOOSE of 5)

</code></pre>
</div>  


<!-- </details> -->
