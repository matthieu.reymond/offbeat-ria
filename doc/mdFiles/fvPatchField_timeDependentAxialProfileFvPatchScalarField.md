### User documentation for `timeDependentAxialProfileT` fvPatchField class {#fvPatchField_timeDependentAxialProfileFvPatchScalarField}

For a list of fvPatchField classes specifically developed for OFFBEAT, please refer to the [fieldsAndBCs](fieldsAndBCs.html) page.

<!-- <details>
  <summary style="font-size: 0.9rem; font-weight: bold;"> Description </summary> -->

#### Summary

The `timeDependentAxialProfileT` boundary condition allows to impose a time dependent temperature axial profile on a patch. 
If an oxide layer is present, an additional thermal resistance corresponding to the oxide is considered in order to set the patch temperature. The oxideLayer temperature needs to be specified by the user.

<div class="border-box" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color:#cfe3f5; color:#05134a'>
<b>Note</b>
<br> - The axial locations provided to this boundary conditions are NOT normalized but absolute values expressed in \f$m\f$.
<br> - The temperature values provided to this boundary conditions are expressed in \f$K\f$.
</div>

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Examples </summary> -->

#### List of options

The `timeDependentAxialProfileT` boundary condition requires the user to specify a few additional parameters in the patch dictionary:

- <b><c>axialProfileDict</c></b> - Dictionary containing the information related to the time-dependent axial profile.
- <b><c>timeInterpolationMethod</c></b> - Inside the dictionary `axialProfileDict`, a keyword for selecting the type of time interpolation (`linear` or `step`). **By default is `linear`**.
- <b><c>axialLocations</c></b> - Inside the dictionary `axialProfileDict`, a list of axial location at witch the axial profile values are given. **The axial values are NOT normalized**.
- <b><c>data</c></b> - Inside the dictionary `axialProfileDict`, a table of the axial profile values in K (one list per time point; in each list one value per axial location).
- <b><c>axialInterpolationMethod</c></b> - Inside the dictionary `axialProfileDict`, a keyword for selecting the type of axial interpolation (`linear` or `step`). **By default is `linear`**.

Being a `fixedTemperature` BC, the user should also specify:

- <b><c>outerOxideTemperature</c></b> - If the corrosion model is present, this keyword specifies the initial outer temperature (i.e. the one outside the oxide layer). **If not present, the initial oxide outer temperature is set to "value"**
- <b><c>value</c></b> - The initial **temperature** value (in case of oxidation model, it is the outer temperature of the metallic portion of the body).  

#### Examples

To apply the `timeDependentAxialProfileT` boundary condition to a given patch, the following example can be used as a template:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
  claddingOuterSurface
  {
      type            timeDependentAxialProfileT;
      axialProfileDict
      {
          timePoints  (0 1e2 2e2 ... );
          timeInterpolationMethod  linear;

          // Note: axial locations are NOT normalized
          axialLocations ( 0 0.5 1 1.5 ... );
          axialInterpolationMethod linear;
          data (
                  (400 420 430 425 ... )
                  (450 470 480 475 ... )
                  (440 460 470 465 ... )
                  :
                  :
                  :
                );
      }
      value           uniform 300;

      // If the corrosion model is present, the initial outer temperature 
      // (i.e. the one outside the oxide layer) can be specified with the 
      // following keyword. Otherwise, it is set equal to "values"
      outerOxideTemperature           uniform 300;
  }

</code></pre>
</div>  

<!-- </details> -->    
    