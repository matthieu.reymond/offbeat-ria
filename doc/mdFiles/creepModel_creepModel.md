### User documentation for `creepModel` class {#creepModel_creepModel}

The `creepModel` class is the mother class for all the creep models implemented in OFFBEAT.

The following creep models are currently available in OFFBEAT:
- [powerLaw](@ref Foam.powerLaw) to model the creep strain rate with a power law with user provided parameters.

- [MatproCreepModel](@ref Foam.MatproCreepModel) for the thermal and irradiation creep of UO2 fuel.

- [TobbeCreepModel](@ref Foam.TobbeCreepModel) for the thermal and irradiation creep of 15-15Ti steel for fast reactors applications
- [TobbeAIM1CreepModel](@ref Foam.TobbeAIM1CreepModel) for the thermal and irradiation creep of the AIM1 specific class of 15-15Ti steel
- [TobbeDINCreepModel](@ref Foam.TobbeDINCreepModel) for the thermal and irradiation creep of the DIN 1.4970m specific class of 15-15Ti steel

- [LimbackCreepModel](@ref Foam.LimbackCreepModel) for the thermal and irradiation creep of zircaloy for LWR applications.
- [LimbackLOCA](@ref Foam.LimbackLOCA) for the thermal and irradiation creep of zircaloy-4 during Loss Of Coolant Accident transients.


