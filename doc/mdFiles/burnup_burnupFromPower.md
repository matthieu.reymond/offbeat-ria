### User documentation for `fromPower` burnup class {#burnup_burnupFromPower}

For general instructions on the modeling of burnup in OFFBEAT see [here](burnup.html).

The `fromPower` burnup class can be used to track the evolution of the local burnup in <code><b>(MWd/MT<sub>oxide</sub>)</b></code>.

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Formulation </summary> -->

#### Formulation

At each time step, the updated burnup is calculated as:

$$
\begin{aligned} 
Bu = Bu^0 + \frac{Q \cdot \Delta t}{1000 \cdot \rho}
\end{aligned}
$$

where:

- \f$Bu^0\f$ is the old local burnup value, i.e. at the end of the previous time step, in MWd/MT\f$_{oxide}\f$
- \f$Q\f$ is the power density in W/m\f$^3\f$
- \f$\rho\f$ is density in kg/m\f$^3\f$
- \f$\Delta t\f$ is time step in seconds.

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Usage </summary> -->

#### Usage

To use the `fromPower` burnup model in OFFBEAT, the user needs to specify it in the `solverDict` dictionary using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	burnup fromPower;

</code></pre>
</div>

The `fromPower` burnup model has at the moment only a single additional parameter that the user <u>might</u> specify in the `burnupOptions` sub-dictionary:

- <b><c>heatSourceName</c></b> - The name of the heat source field. <b>By default this is set as `Q` in OFFBEAT</b>. This keyword could be useful if the power density field (for instance calculted via a separate code) is provided via a different/external heat source model.

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Examples </summary> -->

#### Examples

Here is a code snippet of the `solverDict` to be used for activating the `fromPower` burnup model

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	burnup fromPower;

	burnupOptions
	{
    	// Name of the heat source field (set to "Q" by default)
        heatSourceName  "Q";
	}

</code></pre>
</div>

<!-- </details> -->