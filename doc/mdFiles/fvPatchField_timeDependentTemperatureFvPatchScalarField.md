### User documentation for `timeDependentTemperature` fvPatchField class {#fvPatchField_timeDependentTemperatureFvPatchScalarField}

For a list of fvPatchField classes specifically developed for OFFBEAT, please refer to the [fieldsAndBCs](fieldsAndBCs.html) page.

<!-- <details>
  <summary style="font-size: 0.9rem; font-weight: bold;"> Description </summary> -->

#### Summary

The `timeDependentTemperature` boundary condition allows to impose a time dependent temperature on a patch. 
If an oxide layer is present, an additional thermal resistance corresponding to the oxide is considered in order to set the patch temperature. The oxideLayer temperature needs to be specified by the user.

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Examples </summary> -->

#### List of options

The `timeDependentTemperature` boundary condition requires the user to specify a few additional parameters in the patch dictionary:

- <b><c>temperatureSeries</c></b> - Dictionary containing the information related to the time-dependent temperature.
- <b><c>values</c></b> - Inside the dictionary `temperatureSeries`, a list of tuples of (time, temperature) values.
- <b><c>interpolationScheme</c></b> - Inside the dictionary `temperatureSeries`, a keyword for selecting the type of interpolation. **By default is `linear`**.

Being a `fixedTemperature` BC, the user should also specify:

- <b><c>outerOxideTemperature</c></b> - If the corrosion model is present, this keyword specifies the initial outer temperature (i.e. the one outside the oxide layer). **If not present, the initial oxide outer temperature is set to "value"**
- <b><c>value</c></b> - The initial **temperature** value (in case of oxidation model, it is the outer temperature of the metallic portion of the body).    

#### Examples

To apply the `timeDependentTemperature` boundary condition to a given patch, the following example can be used as a template:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
  claddingOuterSurface
  {
    type            timeDependentTemperature;

    // Time dependent temperature
    temperatureSeries
    {
        // Including an external file containing the time dependent table
        // for a more readable input
        values #include    "$FOAM_CASE/constant/lists/claddingTemperature";

        // optional out-of-bounds handling
        outOfBounds         clamp;

        // optional interpolation method
        interpolationScheme linear;
    }

    // If the corrosion model is present, the initial outer temperature 
    // (i.e. the one outside the oxide layer) can be specified with the 
    // following keyword. Otherwise, it is set equal to "values"
    outerOxideTemperature           uniform 300;
  }

</code></pre>
</div>  
An example of the time dependent table included in the previous example under the keyword `values` is given hereafter. The first column represents time units, while the second columns represent temperatures (K).

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
  (
    (0      300.0)
    (3600   500.0)
    (7200   500.0)
    (10800  300.0)
  );

</code></pre>
</div>  

<!-- </details> -->    
    