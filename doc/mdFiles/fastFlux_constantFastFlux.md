### User documentation for `fromLatestTime` fastFlux class {#fastFlux_constantFastFlux}

For general instructions on the modeling of the fast flux and fast fluence in OFFBEAT see [here](fastFlux.html).

The `fromLatestTime` fastFlux class in OFFBEAT allows you to set the fast flux and fast fluence fields in files located the starting time folder. If the <c>fastFlux</c> and <c>fastFluence</c> files are present in the starting time folder, the internal field and boundary conditions are set accordingly. Otherwise, the fields are set to 0 <c>n/cm<sup>2</sup>/s</c> and 0 <c>n/cm<sup>2</sup></c> respectively in each cell, with zeroGradient BCs in all non-empty/-wedge patches. Note that when selecting this fast flux model, the fast flux field will remain the same as it was defined in the starting time folder (or set by default).

<div class="border-box" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color:#cfe3f5; color:#05134a'>
<b>Note on fast fluence field</b>
<br><br>Even when "fromLatestTime" is selected, the fast fluence keeps being updated at each time step as:
<br>$$fastFluence = fastFluence.oldTime() + fastFlux \cdot \Delta t$$
</div>

<!-- <details> -->
	<!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Usage </summary> -->

#### Usage

To use the `fromLatestTime` fast flux model in OFFBEAT, the user needs to specify it in the `solverDict` dictionary using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	fastFlux fromLatestTime;
		
</code></pre>
</div>	

<!-- </details> -->
