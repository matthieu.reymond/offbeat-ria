### User documentation for `conductivityPARFUMEBuffer` class {#conductivity_conductivityPARFUMEBuffer}

The `conductivityPARFUMEBuffer` model conductivity of Buffer material by the model taken from the code PARFUME.

#### Formulation

The thermal conductivity of buffer is given by the correlation below:

$$
\begin{aligned}
k = \frac{k_{init} k_{theo}(\rho_{theo}-\rho_{init})}{k_{theo}(\rho_{theo}-\rho)+k_{init}(\rho-\rho_{init})}
\end{aligned}
$$

where:

- \\(k_{init} \\) is the inital thermal conductivity of the buffer,
- \\(k_{theo} \\) is the thermal conductivity of the buffer at its theoretical density,
- \\(\rho_{theo} \\) is the theoretical density,
- \\(\rho_{init} \\) is the inital density,
- \\(\rho \\) is the current density of buffer.



#### Usage

To use `conductivityPARFUMEBuffer` in OFFBEAT, you just need to specify the keyword **BufferPARFUME** for `conductivityModel`.

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    materials
    {
        //- Name of cellZone with buffer material model
        Buffer
        {
            material buffer;
            conductivityModel  BufferPARFUME;
        }

        ...

    }

</code></pre>
</div>

#### Input parameters

|                |   Description                   |     Default            |
|----------------|---------------------------------|------------------------|
|densityName     |Name used for the density field  |rho       |
|initialDensity  |Initial density of Buffer(\f$kg/m^3\f$)        |1000.0 |
|theoreticalDensity |Theoretical density of Buffer(\f$kg/m^{3}\f$) |2250.0 |
|initialConductivity |Initial thermal conductivity of Buffer(\f$W/m/K\f$)|0.5 |
|theoreticalConductivity |Thermal conductivity of Buffer at its theoretical density(\f$W/m/K\f$) |4.0 |
