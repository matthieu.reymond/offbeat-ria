### User documentation for `timeDependentAxialProfile` fastFlux class {#fastFlux_timeDependentAxialProfile}

For general instructions on the modeling of the fast flux and fast fluence in OFFBEAT see [here](fastFlux.html).

The `timeDependentAxialProfile` class in OFFBEAT allows you to set a time dependent average fast flux.

It is also possible to apply a an axial profile to the fast flux. Note that even if the fast flux is constant over time, the axial profile might change over time, depending on the profile type chosen by the user.

The fast fluence will evolve in time according to the local value of the fast flux.

<!-- <details> -->
	<!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Formulation </summary> -->

#### Formulation  

The final local fast flux value is derived as a combination of rod-average fast flux and axial profile, as follows: 
$$
\begin{aligned} 
fastFlux(t, z) = \phi(t) \cdot g(z,t)
\end{aligned}
$$

where:

- \f$z\f$ is the relative axial position (0...1)
- \f$t\f$ is the current time
- \f$g(z,t)\f$ is the axial profile
- \f$\phi\f$ is the rod-average fast flux in <c>n/cm<sup>2</sup>/s</c>.

The axial profile is expected to be normalized to 1.

<!-- </details> -->
<!-- 
<details>
	<summary style="font-size: 0.9rem; font-weight: bold;"> Usage </summary> -->

#### Usage

To use the `timeDependentAxialProfile` fast flux model in OFFBEAT, the user needs to specify it in the `solverDict` dictionary using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	fastFlux timeDependentAxialProfile;

</code></pre>
</div>

The `timeDependentAxialProfile` fast flux model requires the user to specify a few additional parameters in the `fastFluxOptions` sub-dictionary:

- <b><c>timePoints</c></b> - A list of time values at which the $\phi$ is provided. The time unit depend on the userTime selected by the user (seconds by default).
- <b><c>fastFlux</c></b> - A list of \f$\phi\f$ values in \f$n/cm^2/s\f$, one value per time-point.
- <b><c>timeInterpolationMethod</c></b> - Select the time interpolation method for time steps that fall in between the time points indicated in the timePoints list.
- <b><c>materials</c></b> - A list of material (or cellZones names) where the fast flux source model applies.
- <b><c>axialProfile</c></b> - A sub-dictionary that specifies the type of axial profile.

<!-- </details> -->

<!-- <details> -->
	<!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Examples </summary> -->

#### Examples

Here is a code snippet of the `solverDict` to be used for the case of a fast flux that ramps from 0 to 1e13 <c>n/cm<sup>2</sup>/s</c> in 1hr time, remains constant for a year, and then ramps down to 0 in 1 hr. 
This particular example considers  flat (i.e. uniform) axial profile.

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	fastFlux timeDependentAxialProfile;

	fastFluxOptions
	{
		//           t0  1hr    1yr       1yr+1hr
		timePoints  ( 0   3600   31536000  31539600);
		fastFlux    ( 0   1e13   1e13      0       );

		timeInterpolationMethod linear; //step;

		materials ( fuel cladding);

		axialProfile
		{        
				type flat;
		}
	}

</code></pre>
</div>

For a time-dependent axial profile, one can modify the `axialProfile` sub-dictionary as follows:

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	fastFluxOptions
	{
		// .. The rest of the dictionary may remain unchanged

		axialProfile
		{        
			// Example: time dependent axial radial profile. 
			// The user provides a 2D table (z-vs-time) of normalized fast flux
			// values (normalized to the average).
			// Other options might be available (see axialProfile class).     
			type            timeDependentTabulated;

			// List of time points for axial profile
			timePoints      ( 0 3600 31539600 );

			// List of axial locations for axial profile
			axialLocations  ( 0.0 0.5 1.0 );

			// 2D table for axial profile (z is x axis; time is y axis)
			data          
			(
					( 1.0 1.0 1.0 )
					( 0.5 1.0 1.5 )
					( 0.8 1.1 1.0 ) 
			);

			// Select the axial interpolation method
			axialInterpolationMethod    linear;

			// Select the time interpolation method for time steps that fall in between
			// the time points indicated in the timePoints list above
			timeInterpolationMethod     linear;
		}

	}

</code></pre>
</div>  

As the time point or data lists might be quite long for a realistic history, making the solver dictionary less readable, the user can create separate files and include them in the fastFluxOptions dictionary using the #include command. In the example below the files are stored in the folder <c>constant/lists</c>.
E.g.:

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code> 
	fastFluxOptions
	{
		timePoints  #include "$FOAM_CASE/constant/lists/timePoints";
		fastFlux    #include "$FOAM_CASE/constant/lists/avgPower";
		timeInterpolationMethod linear;

		...
	}

</code></pre>
</div> 


<!-- </details> -->
