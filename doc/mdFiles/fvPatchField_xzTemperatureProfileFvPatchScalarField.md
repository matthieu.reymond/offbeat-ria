### User documentation for `xzTemperatureProfile` fvPatchField class {#fvPatchField_xzTemperatureProfileFvPatchScalarField}

For a list of fvPatchField classes specifically developed for OFFBEAT, please refer to the [fieldsAndBCs](fieldsAndBCs.html) page.

<!-- <details>
  <summary style="font-size: 0.9rem; font-weight: bold;"> Description </summary> -->

#### Summary

The `xzTemperatureProfile` boundary condition allows to impose a temperature profile constant in time but varying along x and z directions. If an oxide layer is present, an additional thermal resistance corresponding to the oxide is considered in order to set the patch temperature. The oxideLayer temperature needs to be specified by the user.

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Examples </summary> -->

#### List of options

The `xzTemperatureProfile` boundary condition requires the user to specify a few additional parameters in the patch dictionary:

- <b><c>axialProfileDict</c></b> - Dictionary containing the information related to the xz-dependent axial profile.
- <b><c>xInterpolationMethod</c></b> - Inside the dictionary `axialProfileDict`, a keyword for selecting the type of x-interpolation (`linear` or `step`). **By default is `linear`**.
- <b><c>xLocations</c></b> - Inside the dictionary `axialProfileDict`, a list of x-location at witch the axial profile values are given. **The x-values are NOT normalized**.
- <b><c>data</c></b> - Inside the dictionary `axialProfileDict`, a table of the axial profile values in K (one list per x location; in each list one value per axial location).
- <b><c>zLocations</c></b> - Inside the dictionary `axialProfileDict`, a list of axial z-location at witch the axial profile values are given. **The axial values are NOT normalized**.
- <b><c>axialInterpolationMethod</c></b> - Inside the dictionary `axialProfileDict`, a keyword for selecting the type of axial interpolation (`linear` or `step`). **By default is `linear`**.

Being a `fixedTemperature` BC, the user should also specify:

- <b><c>outerOxideTemperature</c></b> - If the corrosion model is present, this keyword specifies the initial outer temperature (i.e. the one outside the oxide layer). **If not present, the initial oxide outer temperature is set to "value"**
- <b><c>value</c></b> - The initial **temperature** value (in case of oxidation model, it is the outer temperature of the metallic portion of the body).    

#### Examples

To apply the `xzTemperatureProfile` boundary condition to a given patch, the following example can be used as a template:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>

  claddingOuterSurface
  {
      type            xzTemperatureProfile;
      axialProfileDict
      {
          xLocations  ( -0.005 0.005 );
          xInterpolationMethod linear;

          zLocations ( 0 4 );
          zInterpolationMethod linear;

          data 
          (
            //  T @ z=0   T @ z=4
              (   300       400   ) // T @ x = -0.005
              (   300       500   ) // T @ x =  0.005
          );
      }

      value           uniform 300;

      // If the corrosion model is present, the initial outer temperature 
      // (i.e. the one outside the oxide layer) can be specified with the 
      // following keyword. Otherwise, it is set equal to "value"
      outerOxideTemperature           uniform 300;
  }

</code></pre>
</div>  

<!-- </details> -->    
    