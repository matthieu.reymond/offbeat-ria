### User documentation for `elasticity` class {#constitutiveLaw_elasticity}

For general instructions on the modeling of the rheology of a material in OFFBEAT [here](rheologyhtml).

The `elasticity` constitutive law implements the Hooke linear mechanical behavior:
$$
	\underline{\sigma} = \textbf{E} : \varepsilon^{el} = 2\mu \times \underline{\varepsilon}^{el} + \lambda \times tr(\underline{\varepsilon}^{el})
$$

where:
- \\( E \\) is the elastic modulus which can be splitted into:
	- \\( \mu \\), the first Lamé parameter and
	- \\( \lambda\\), the second Lamé parameter.
- \\( \varepsilon^{el}\\) is the elastic strain tensor.


#### Usage  
To use the `elasticity` constitutive law in OFFBEAT, you will need to specify it in the `solverDict` dictionary using the following syntax:
<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>	
<pre style="margin: 0;"><code>
rheology byMaterial;
...
materials
{
    fuel
    {
        material UO2;
        ...
        rheologyModel elasticity;
    }
}
</code></pre>
</div>  


