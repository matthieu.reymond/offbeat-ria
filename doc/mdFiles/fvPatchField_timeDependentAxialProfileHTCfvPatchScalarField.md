### User documentation for `timeDependentAxialProfileHTC` fvPatchField class {#fvPatchField_timeDependentAxialProfileHTCfvPatchScalarField}

For a list of fvPatchField classes specifically developed for OFFBEAT, please refer to the [fieldsAndBCs](fieldsAndBCs.html) page.

<!-- <details>
  <summary style="font-size: 0.9rem; font-weight: bold;"> Description </summary> -->

#### Summary

The `timeDependentAxialProfileHTC` boundary condition allows to simulate an heat sink boundary defined by a convective heat transfer coefficient \f$h\f$ and a sink temperature \f$T_0\f$ that vary axially and during time.

This boundary condition computes the temperature \f$T\f$ to be applied to the patch using the following expression for heat flux \f$q"\f$:

\f[
  q" = h\cdot(T-T_0)
\f]

Where \f$q"\f$ is computed from the Fourier's Law:

\f[
  q" = -k\cdot \nabla T
\f]

<div class="border-box" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color:#cfe3f5; color:#05134a'>
<b>Note</b>
<br> - The axial locations provided to this boundary conditions are NOT normalized but absolute values expressed in \f$m\f$.
<br> - The \f$T_0\f$ values provided to this boundary conditions are expressed in \f$K\f$.
<br> - The \f$h\f$ values provided to this boundary conditions are expressed in \f$\frac{W}{m^2 K}\f$.
</div>

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Examples </summary> -->

#### Examples

To apply the `timeDependentAxialProfileHTC` boundary condition to a given patch, the following example can be used as a template:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
  cladOuter
  {
      type            timeDependentAxialProfileHTC;
      axialProfileDict
      {
          timePoints     (0 1e3 1.261e8);
          axialLocations (0.0 1.0 2.0 3.2);
          hData          (
                              (5e3 5.01e3 5.02e3 5.01e3)
                              (5e3 5.01e3 5.02e3 5.01e3)
                              (5e3 5.01e3 5.02e3 5.01e3)
                          );
          T0Data          (
                              (350 370 380 365)
                              (360 380 370 360)
                              (340 360 390 376)
                          );
          axialInterpolationMethod "linear";
          timeInterpolationMethod "linear";
      }
      value           $internalField;
  }    
</code></pre>
</div>  

<!-- </details> -->    
    