### User documentation for `fromLatestTime` neutronics class {#neutronicsSubSolver_neutronicsSubSolver}

For general instructions on the modeling of neutronics in OFFBEAT see [here](neutronics.html).

The `fromLatestTime` neutronics solver class in OFFBEAT allows you to entirely neglects the evolution of neutron flux over time. No additional field is created. Essentially, this class is equivalent to what in OFFBEAT is a <c>none</c> class for other models (e.g. fission gas release).

#### Usage  

To use the `fromLatestTime` neutronics solver in OFFBEAT, the user will need to specify it in the `solverDict` dictionary using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
   neutronicsSolver fromLatestTime;

</code></pre>
</div>
