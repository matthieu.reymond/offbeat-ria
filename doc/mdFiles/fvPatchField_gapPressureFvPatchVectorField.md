### User documentation for `gapPressure` fvPatchField class {#fvPatchField_gapPressureFvPatchVectorField}

For a list of fvPatchField classes specifically developed for OFFBEAT, please refer to the [fieldsAndBCs](fieldsAndBCs.html) page.

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Description </summary> -->

#### Description

The `gapPressure` boundary condition applies the gap pressure to the selected patch. It is derived form the `tractionDisplacement` BC so it shares its general mechanicsm, only that this time the user does not need to specify the traction vector or pressure.

Note that this BC is specifically designed to work with a `gapGas` model that provides the gap pressure at every iteration.

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> List of options </summary> -->

#### List of options  

The `gapPressure` boundary condition requires fewer options than its mother-class `tractionDisplacement` as the traction vector is a consequence of the gap pressure.

Thus, the user to specify the following parameter in the patch dictionary:

- <b><c>planeStrain</c></b> - Activate or deactivate the plane strain approximation for the normal stress at the boundary. **By default, it is set to `false`**. When activated, the normal component of the boundary normal gradient (and thus the normal strain) is assumed constant across the last layer of cells. This approximation is useful for 2D r-z meshes, where the axial discretization of the mesh may not capture the rapid variation of the axial stresses close to the boundary.
- <b><c>relax</c></b> - The relaxation factor for the gradient update. **By default, it is set to `1.0`.**
- <b><c>value</c></b> - The initial **displacement** value (not stress value).

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Examples </summary> -->

#### Examples

To apply the `gapPressure` boundary condition to a given patch, you need to specify its typename in the patch definition in the boundary condition file (typically either `0/D` or `0/DD` in case of a simulation that involves incremental mechanical solvers). 
Here is an example where `gapPressure` is applied to the `fuelInner` patch (make sure to change `fuelInner` with the appropriate boundary patch name in your specific case):

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    fuelInner
    {
        type            gapPressure;

        // Activate plane strain approximation for the normal stress at the 
        // boundary. It is false by default.
        planeStrain     false;

        // Relaxation factor for the gradient update
        relax           1.0;

        // Initial value
        value           $internalField;
    }

</code></pre>
</div>  

<!-- </details> -->
