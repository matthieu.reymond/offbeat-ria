### User documentation for `none` fastFlux class {#fastFlux_fastFlux}

For general instructions on the modeling of the fast flux and fast fluence in OFFBEAT see [here](fastFlux.html).

As the name implies, the `none` fast flux class in OFFBEAT allows you to switch off the fast flux and fast fluence models and have a simulation where these fields are not considered.
This means that the fast flux and fast fluence fields <code>fastFlux</code> and <code>fastFluence</code> are not created.

<div class="border-box" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color:#cfe3f5; color:#05134a'>
<b>Note</b>
<br><br>The abscence of the <code>fastFlux</code> and <code>fastFluence</code> fields might create conflicts with other models that partially or entirely depends on them (e.g. some of the material properties models).
<br>If you want to use those models bust still neglect the fast flux and fluence evolution, use the [fromLatestTime](@ref Foam.constantFastFlux) model.
</div>

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Usage </summary> -->

#### Usage

To use the `none` fast flux model in OFFBEAT, the user needs to specify it in the `solverDict` dictionary using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    fastFlux none;

</code></pre>
</div>	


<!-- </details> -->
