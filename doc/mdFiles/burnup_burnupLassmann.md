### User documentation for `Lassmann` burnup class {#burnup_burnupLassmann}

For general instructions on the modeling of burnup in OFFBEAT see [here](burnup.html).

The `Lassmann` burnup class in OFFBEAT is inspired by the work of Lassmann et al., "The radial distribution of plutonium in high burnup UO2 fuels" Journal of Nuclear Materials.
The class extends the functionality of the base `fromPower` burnup class as it serves as a simplified depletion module incorporating a small set of relevant nuclides and solving the Bateman equation for their time evolution.
The clais designed to provide a more accurate representation of the power distribution within the fuel by considering the spatial variation of power density across the radial domain. 

<div class="border-box" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color:#cfe3f5; color:#05134a'>
<b>Note</b>
<br><br>The `Lassmann` burnup model is designed to be used with axially symmetric models or 3D models where there is no significant change in power/nuclide along the azimuthal angle. 
For each slice, the form factor in the fuel will only be calculated along the radius, with no variation alon the azimuthal angle.
<br><br>Also, the models require the subdivision of the fuel rod into axial slices. Thus one should activate the `sliceMapper` model in OFFBEAT.
<br><br>Finally, the `neutronicsSolver` should be activated as a way of providing the neutron flux distribution to the depletion module.
</div>


<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Formulation </summary> -->
#### Formulation

First, at each time step, the updated _average burnup for each fuel slice_ is calculated as:

$$
\begin{aligned} 
Bu = Bu^0 + \frac{Q \cdot \Delta t}{1000 \cdot \rho}
\end{aligned}
$$

where:\

- \f$Bu^0\f$ is the old local burnup value, i.e. at the end of the previous time step, in MWd/MT\f$_{oxide}\f$
- \f$Q\f$ is the power density in W/m\f$^3\f$
- \f$\rho\f$ is density in kg/m\f$^3\f$
- \f$\Delta t\f$ is time step in seconds

Then, a simplified version of the Bateman equations is solved for a small set of relevant nuclides. At the moment: U235, U236, U238, Np237, Pu238, Pu239, Pu240, Pu241, Pu242, Am241, Am243, Cm242, Cm243, Cm244.
These nuclides are chosen based on their significance for the power profile.

The updated values of the fissile nuclide distribution in each cell are combined with the flux distribution obtained from the `neutronicsSolver` model to obtain the power profile in a given slice:

$$
\begin{aligned} 
Q(r) = \sum_f \left( {N_f(r) \sigma_f(r)} \phi(r) \right)
\end{aligned}
$$

where \f$\phi\f$ is the flux, N\f$_f\f$ is the nuclide concentration of a fissile isotope and \f$\sigma_f\f$ is the fissile cross section, 

The local power density is then used to calculate the local burnup increment, which quantifies the amount of fuel burnup that occurs in a given region during a specific time step. 
This increment is used to update the nuclide distribution for the next iteration of the Lassman model.

For a detailed explanation of the Bateman equation and its application in burnup calculations, it is recommended to refer to relevant nuclear engineering literature or online resources.

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Usage </summary> -->

#### Usage  

To use the `Lassmann` burnup model in OFFBEAT, you will need to specify it in the `solverDict` dictionary using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	burnup Lassmann;

</code></pre>
</div>

The `Lassmann` burnup model has at the moment only a single additional parameter that the user _can_ specify in the `burnupOptions` sub-dictionary:

- <b><c>convergePrecision</c></b> - Parameter that controls the convergence precision for the burnup calculations. **By default, it is set to 1e-2 (which is the suggested value).** 

<!-- </details> -->

Also, the `Lassmann` burnup model requires that the _specific material dictionary_ for all fuel-type materials (e.g., UO2 or MOX) includes:

- <b><c>enrichment</c></b> - The enrichment of fissile material, in fraction (i.e. 0.025 for a 2.5% enrichment).
- <b><c>densityFraction</c></b> - The fraction of theoretical density of the fuel (e.g. 0.95).

Finally, the `Lassmann` burnup model requires the use of a `neutronicsSolver` (for instance, the `diffusion` solver) that calculates the neutron flux distribution in the fuel.

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Examples </summary> -->

#### Examples

Here is an example of the `solverDict` to be used for activating the `Lassmann` burnup model

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	burnup Lassmann;
	neutronicsSolver diffusion;

	burnupOptions
	{
    	// Name of the heat source field (set to "Q" by default)
      heatSourceName  "Q";

      // Convergence criterion for depletion iterations. By default set to 1e-2.
      convergencePrecistion 1e-2;
	}

	materials
	{
		fuel
		{
			material uo2;
			// ... remaining part of the dictionary

			enrichment 0.025;
			densityFraction 0.95;
		}

	}

</code></pre>
</div>
<!-- </details> -->