### User documentation for `constantCreepPrincipalStress` class {#creepModel_constantCreepPrincipalStress}

#### Description
The `constantCreepPrincipalStress` implements the computation of the creep strain rate in the following way:
\f[
	\dot{\varepsilon}^{creep} = K\times[\sigma_1 - \nu_v\times(\sigma_2 + \sigma_3)]\times 
\f]
where:
- \\(\sigma_i) \\) are the principal stresses (egein values of the stress tensor)
- \\(\nu_c \\) is the Poisson ratio for creep
- \\(\K \\) is an user provided creep coefficient.

This class is used to verify the OFFBEAT results with IAEA CPR-6 benchmark for TRISO applications.

#### Usage
To use the `constantCreepPrincipalStress` creep model in OFFBEAT, you will need to specify it in the `solverDict` using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
rheology byMaterial;
...
materials
{
    ...
    "IPyC|OPyC"
    {
        material PyC;
        ...
        rheologyModel  misesPlasticCreep;
        rheologyModelOptions
        {
            yieldStressModel    plasticStrainVsYieldStress;
            plasticStrainVsYieldStress table
            (
                (0    160e6)
            );
            creepModel constantCreepPrincipalStress;
            fluxConversionFactor       1.0;
            creepCoefficient         4.93e-4;
            poissonRationForCreep    0.4;
            relax 1;
        }
    }
}
</code></pre>
</div>	