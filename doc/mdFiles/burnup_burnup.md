### User documentation for `none` burnup class {#burnup_burnup}

For general instructions on the modeling of burnup in OFFBEAT see [here](burnup.html).

As the name implies, the `none` burnup class in OFFBEAT allows you to switch off the burnup model and have a simulation where burnup is not considered.
This means that the burnup field <code>Bu</code> is not created.

<div class="border-box" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color:#cfe3f5; color:#05134a'>
<b>Note</b>
<br>The abscence of the <code>Bu</code> field might create conflicts with other models that partially or entirely depends on the burnup (e.g. fuel conductivity).
<br>If you want to use those models bust still neglect the burnup evolution, use the [fromLatestTime](@ref Foam.constantBurnup) model.
</div>

<!-- <details> -->

#### Usage

To use the `none` burnup model in OFFBEAT, the user needs to specify it in the `solverDict` dictionary using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'> 
<pre style="margin: 0;"><code>
    burnup none;

</code></pre>
</div>  



<!-- </details> -->
