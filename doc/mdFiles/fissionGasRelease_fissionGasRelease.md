### User documentation for `none` fission gas release  class {#fissionGasRelease_fissionGasRelease}

For general instructions on the modeling of the fission gas release in OFFBEAT see [here](fgr.html).

As the name implies, the `none` fission gas release class in OFFBEAT allows you to switch off the fgr models and have a simulation where fgr production, release and related gaseous swelling are not considered.
This means that no additional fields are created.

<div class="border-box" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color:#cfe3f5; color:#05134a'>
<b>Note on additional fields</b>
<br><br>Although with the `none` fgr class no additional fields are created for the fission gases itself (e.g. concentration, release fraction etc.) the basic fgr class generates the following fields:
<br><br>- `grainRadius` and `oxygenMetalRatio`
<br>- `interGranularSwelling` and `interGranularSwelling`
<br><br>While this specific aspect might change in future version of OFFBEAT, at the moment selecting `none` means that the previously mentioned fields will be created and will remain constant during the simulation.
</div>

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Usage </summary> -->

#### Usage

To use the `none` fgr model in OFFBEAT, the user needs to specify it in the `solverDict` dictionary using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
  fgr none;

</code></pre>
</div>	


<!-- </details> -->