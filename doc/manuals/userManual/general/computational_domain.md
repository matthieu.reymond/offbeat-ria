# Computational Domain, Mesh and Boundary Definitions {#computationalDomain}

OpenFOAM and OFFBEAT operate using a cell-centered finite-volume framework. The computational domain is decomposed into cells and faces. While OpenFOAM supports arbitrary unstructured polyhedral meshes, hexagonal meshes are generally used for OFFBEAT simulations due to the relative simplicity of the fuel rod geometry. It is important to note that meshes are always 3-D, however 2-D and 1-D problems can be run effectively using suitable boundary conditions. For example, for 2-D axisymmetric problems, wedge boundaries are used in the \f$\theta\f$ direction.

The following figure shows an example of the computational domain, mesh and boundary definitions for a typical LWR fuel rod model, using a 2-D axisymmetric model. Here the fuel is modelled as a smeared column where the individual pellets have not been resolved.

\image html computational_domain.svg "Sample 2-D Axisymmetric Computational Domain for a LWR Fuel Rod" width=50%

Different materials of the computational domain (fuel and cladding) are included as `cellZones`. These are assigned in different ways for different meshing tools. `blockMesh`, for example, allows one to assign a name to each block.

Experience suggests that around 30 radial nodes in the fuel and 20 in the cladding are more than sufficient for most problems. In the axial direction, typical OFFBEAT models use one mesh cell per fuel pellet although this has not been extensively tested.

***

Return to [Case Folder Structure and Workflow ](@ref general)
