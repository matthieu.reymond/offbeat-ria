# Displacement Boundary Conditions {#displacementBCs}

This section summarizes the main displacement boundary conditions currently available in OFFBEAT. All the available options are quickly visible using the "banana method", i.e. using a dummy keywords (e.g "banana") for the boundary condition type. Running the case OpenFOAM will detect the mistake and propose a set of possible valid alternatives to the dummy keyword.


### Fixed value (Dirichlet) boundary conditions
BCs that impose a displacement value (or time list) on a patch.

| Boundary Condition Name      | Brief Description                                                                                      | Class                                            |
|------------------------------|--------------------------------------------------------------------------------------------------------|--------------------------------------------------|
| `fixedDisplacement`          | Imposes a fixed displacement on the patch, which can be provided as a fixed value or as a time series. | fixedTemperatureFvPatchScalarField.H             | 
| `fixedDisplacementZeroShear` | Fixes the boundary normal component of the displacement and the shear stress to zero.                  | fixedDisplacementZeroShearFvPatchVectorField.H   |


### Fixed traction (Neumann) boundary conditions

- [tractionDisplacement](@ref Foam.tractionDisplacementFvPatchVectorField) It models a fixed traction + normal pressure boundary condition.
- [coolantPressure](@ref Foam.coolantPressureFvPatchVectorField) It models the force on the patch as a fixed or time-dependent coolant pressure (re-implementation of the mother-class tractionDisplacement).
- [gapPressure](@ref Foam.gapPressureFvPatchVectorField) It applies the gap pressure on the patch.
- [plenumSpringPressure](@ref Foam.plenumSpringPressureFvPatchVectorField) Designed for the top fuel and top cap inner patches, it accounts for plenum pressure and spring.
- [topCladRingPressure](@ref Foam.topCladRingPressureFvPatchVectorField) Designed for the top surface cladding ring when top cap is not modeled, to take into account the combined effect of coolant pressure, gap pressure and spring force.
- [gapContact](@ref Foam.gapContactFvPatchVectorField) BC designed for fuel-clad gap. Applies gas pressure if the gap is not closed and contact p using the penalty method otherwise.


<img src="https://upload.wikimedia.org/wikipedia/commons/1/1a/C%C3%B4ne_orange_-_under_construction.png"  width="100">


***

Return to [Fields and Boundary Conditions](@ref fieldsAndBCs)


