# Heat source model {#heatSource}

The <code><b>heatSource</b></code> class is used to enable the modeling of a heat source in OFFBEAT.

The power density or heat source density field is by default named <code><b>Q</b></code> and is in (<c>W/m<sup>3</sup></c>). 

<div class="border-box" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color:#cfe3f5; color:#05134a'>
<b>Note on heat source modeling</b>

Traditional 1D codes typically require as an input the radially averaged linear heat generation rate (or <b><c>lhgr</c></b>) as a function of time, and often allow the user to provide a axial profile. For 3D codes with arbitrary geometries and unstructured meshes like OFFBEAT, it is less straightforward to define the heat source field or the power density field. 
<br><br> For scenarios where the power density field is not symmetric, the simplest way to define the heat source field is to couple OFFBEAT with a neutronics/multiphysics solver that directly provides the 3D field and use the [fromLatestTime](@ref Foam.heatSource) heat source model. 
Alternatively, one can define the power density field using OpenFOAM tools such as topoSet (e.g. for creating fields of heat source that can be modeled as mathematical functions). 
<br><br> On the other hand, for simulations (even in 3D) where the heat source is assumed to be uniform along the azimuthal angle, one can use the lhgr models developed specifically for OFFBEAT. These models are listed below.
</div>

### Usage

The heat source model <u>must</u> be selected with the <code><b>heatSource</b></code> keyword in the main dictionary of OFFBEAT (i.e. the <code><b>solverDict</b></code> dictionary, located in the <code><b>constant</b></code>  folder). 

Currently OFFBEAT supports the following heat source models:

- [fromLatestTime](@ref Foam.heatSource) - the heat source field  is read from the <code><b>Q</b></code> file in the starting time folder;
- [constantLhgr](@ref Foam.constantLhgr) - it imposes a <b>constant</b> linear heat generation rate (with the possibility of adding a time-dependent radial/axial profile);
- [timeDependentLhgr](@ref Foam.timeDependentLhgr) - it imposes a <b>time-dependent</b> linear heat generation rate (with the possibility of adding a time-dependent radial/axial profile);
- [timeDependentVhgr](@ref Foam.timeDependentVhgr) - it imposes a <b>time-dependent</b> volumetric heat generation rate.

***

Return to [User Manual](@ref userManual)