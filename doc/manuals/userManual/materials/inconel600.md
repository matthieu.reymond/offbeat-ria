# Inconel 600 material model {#inconel600}


Material model for Inconel-600 alloy material (see inconel600.H). The model uses default selections for thermomechanical properties and behavioral models, but different choices can be selected by the user. 

The default selections of the material model are listed in the table below.

Please refer to the header of each listed file for more information on the specific correlation/model.

### Thermomechanical properties

| Thermomechanical property | Default model                 |
|---------------------------|-------------------------------|
| Thermal Conductivity      |            |
| Density                   |         |
| Emissivity                |           |
| Heat Capacity             |      |
| Poisson's Ratio           |    |
| Thermal Expansion         |  |
| Young's Modulus           |      |

<img src="https://upload.wikimedia.org/wikipedia/commons/1/1a/C%C3%B4ne_orange_-_under_construction.png"  width="100">
