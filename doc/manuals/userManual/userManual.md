# User Manual {#userManual}

The usage instructions in this guide assume that the user has a basic understanding of OpenFOAM usage, including the basic workflow (mesh-generation, preprocessing, running solvers and postprocessing). The user should understand the basic dictionary format for OpenFOAM.

<b>We recommend that new users work through the [OpenFOAM v9 User Guide](https://cfd.direct/openfoam/user-guide/) from the OpenFOAM foundation or the equivalent [OpenFOAM v2312 User Guide](https://doc.openfoam.com/2312/) from the ESI group before attempting to use OFFBEAT.</b>

OFFBEAT operation is similar to typical solvers shipped with OpenFOAM (e.g. icoFoam, pisoFoam, etc.) in that the user provides a mesh, control dictionary `controlDict`, solution parameters `fvSolution` and schemes dictionary `fvSchemes` along with an OFFBEAT-specific solver dictionary `solverDict`, and with intial and boundary conditions for the main fields in the initial time step folder (e.g. the folder `0/`).

* [General Instructions](@ref general) 
* [Setting the `solverDict`](@ref solverDict) 
	* [Thermal Solution](@ref thermal) 
	* [Mechanics Solution](@ref mechanics) 
	* [Neutronics Solution](@ref neutronics) 
	* [Elements Transport Solution](@ref elementTransport) 
	* [Gap Gas Model](@ref gapGas) 
	* [Heat Source](@ref heatSource) 
	* [Fast Flux and Fast Fluence](@ref fastFlux) 
	* [Burnup](@ref burnup) 
	* [Fission Gas Release](@ref fgr) 
	* [Material properties](@ref materialProperties) 
	* [Rheology](@ref rheology) 
	* [3-D to 1-D Mapper](@ref mapper) 
* [Material Models](@ref materials)
* [Fields and Boundary Conditions](@ref fieldsAndBCs) 
* [Adaptive Time Step Options](@ref timeStep) 
* [Relaxation Options](@ref relaxation) 
* [Post-Processing](@ref postprocessing)
* [Tutorials](@ref tutorials)

***

Return to [OFFBEAT Documentation](@ref homePage)
