/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2013 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "constantPoissonRatioUO2.H"
#include "addToRunTimeSelectionTable.H"
#include "zeroGradientFvPatchFields.H"
#include "globalFieldLists.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
    defineTypeNameAndDebug(constantPoissonRatioUO2, 0);
    addToRunTimeSelectionTable
    (
        PoissonRatioModel, 
        constantPoissonRatioUO2, 
        dictionary
    );
}

// * * * * * * * * * * * * * Static Member Functions * * * * * * * * * * * * //


// * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //


// * * * * * * * * * * * * Protected Member Functions  * * * * * * * * * * * //


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::constantPoissonRatioUO2::constantPoissonRatioUO2
(
    const fvMesh& mesh,
    const dictionary& dict, 
    const word defaultModel
)
:
    PoissonRatioModel(mesh, dict, defaultModel),       
    PoissonRatioValue_(0.316),
    isotropicCracking_(dict.lookup("isotropicCracking")),
    nCracks_
    (
        createOrLookup<scalar>(mesh, "nCracks", dimless, 0.0,
                zeroGradientFvPatchField<scalar>::typeName)
    ),
    nCracksMax_(1),
    mapper_(nullptr)
{
    if(dict.found("PoissonRatio"))
    {
        const dictionary& PoissonRatioDict = dict.subDict("PoissonRatio");

        PoissonRatioValue_ = 
        PoissonRatioDict.lookupOrDefault<scalar>("PoissonRatioValue", 0.316);
    }

    if(isotropicCracking_)
    {
        dict.lookup("nCracksMax") >> nCracksMax_;
    }
}

// * * * * * * * * * * * * * * * * Selectors * * * * * * * * * * * * * * * * //


// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

Foam::constantPoissonRatioUO2::~constantPoissonRatioUO2()
{}


// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //
void Foam::constantPoissonRatioUO2::correct
(
    scalarField& sf, 
    const scalarField& T, 
    const labelList& addr
)
{
    if(isotropicCracking_)
    {
        if(mapper_ == nullptr)
        {
            mapper_ = &mesh_.lookupObject<sliceMapper>("sliceMapper");
        }

        scalar nSlices(mapper_->nSlices());
        if(nSlices<=0)
        {
            FatalErrorInFunction()
            << "Number of slices cannot be less than 1." << nl
            << "Check that a sliceMapper different than \"none\" is used "
            << "or check the slice definition."
            << abort(FatalError);
        }

        if(!mesh_.foundObject<volScalarField>("lhgr"))
        {
            FatalErrorInFunction()
                << "Linear heat generation rate field (lhgr) not found." << nl
                << "Check if the selected heat source model defines the lhgr "
                << "as this field is needed by the isotropic cracking model." << nl
                << "Alternatively, deactivate the isotropic cracking model."
                << exit(FatalError);
        }

        // Reference to lhgr (in W/m)
        const volScalarField& lhgr(
            mesh_.lookupObject<volScalarField>("lhgr"));

        // Calculate the slice-average values of lhgr (in W/m)
        const scalarField& lhgrSliceAvg(mapper_->sliceAverage(lhgr));

        //- Linear Heat Rate in kW/m for first crack
        scalar lhr0(5);

        //- Number of first cracks
        scalar n0(1);

        //- Asymptotic number of  cracks
        scalar nInf(12);

        //- Exponential period in kW/m
        scalar tau(21);

        forAll(addr, addrI)
        {
            const label& cellI = addr[addrI];
            const label& sliceID = mapper_->sliceID()[cellI];

            //- Linear Heat Rate in kW/m
            scalar lhrSlice(lhgrSliceAvg[sliceID]);

            scalar nCracks
            (
                n0 + (nInf - n0)*(1 - exp(- (lhrSlice/1000 - lhr0)/tau))
            );

            nCracks_.ref()[cellI]= 
            min
            (
                max(nCracks_.internalField()[cellI], nCracks),
                nCracksMax_
            );
        }
    }  

    forAll(addr, i)
    {   
        const label cellI = addr[i];    
        
        sf[cellI] =  PoissonRatioValue_;

        if(isotropicCracking_)
        {
            sf[cellI] =  
            PoissonRatioValue_/(pow(2, nCracks_.internalField()[cellI]) 
            + (pow(2, nCracks_.internalField()[cellI]) - 1)*PoissonRatioValue_);

        }
        else
        {
            sf[cellI] = PoissonRatioValue_;            
        }


    }
}
// ************************************************************************* //
